CPP=g++
CC=gcc
LFLAG=-lm -pg
CFLAG=-MMD -pg#-O3

#====================================

SGA=sga
SGA_CPP=$(wildcard ./src/sga/*.c)
SGA_OBJ=$(addprefix ./obj/sga/,$(notdir $(SGA_CPP:.c=.o)))

#=====

TABU=tabu
TABU_CPP=$(wildcard ./src/tabu/*.c)
TABU_OBJ=$(addprefix ./obj/tabu/,$(notdir $(TABU_CPP:.c=.o)))

#=====

GEN=gen
GEN_CPP=$(wildcard ./src/gen/*.cpp)
GEN_OBJ=$(addprefix ./obj/gen/,$(notdir $(GEN_CPP:.cpp=.o)))

#=====

NAIVE=naive
NAIVE_CPP=$(wildcard ./src/naive/*.c)
NAIVE_OBJ=$(addprefix ./obj/naive/,$(notdir $(NAIVE_CPP:.c=.o)))

#=====

EXES=       \
   $(SGA)   \
   $(GEN)   \
   $(NAIVE) \
   $(TABU)  \

all: ${EXES}

#====================================

$(GEN): $(GEN_OBJ)
	$(CPP) -o $@ $^ $(LFLAG)

$(SGA): $(SGA_OBJ)
	$(CC) -o $@ $^ $(LFLAG)

$(TABU): $(TABU_OBJ)
	$(CC) -o $@ $^ $(LFLAG)

$(NAIVE): $(NAIVE_OBJ)
	$(CC) -o $@ $^ $(LFLAG)

#====================================

obj/%.o: src/%.cpp
	$(CPP) $(CFLAG) -c -o $@ $<

obj/%.o: src/%.c
	$(CC) $(CFLAG) -c -o $@ $<

-include $(SGA_OBJ:.o=.d)
-include $(GEN_OBJ:.o=.d)
-include $(TABU_OBJ:.o=.d)
-include $(NAIVE_OBJ:.o=.d)

#====================================

clean: 
	rm -rf ${EXES} ./obj/*.d \ ./*.png \
   ./obj/*/*.d \
   ${SGA_OBJ}     \
   ${TABU_OBJ}     \
   ${GEN_OBJ}     \
   ${NAIVE_OBJ}     \
   ./*.gif        \
   ./gmon.out               \
   ./out.pdf         \
   #./*.pdf






