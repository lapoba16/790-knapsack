#include <stdio.h>
#include <stdlib.h>

int max(int a, int b) {
   return (a>b) ? a : b;
}

int knapsack(int w, int *wt, int *val, int n) {
   // if end of line
   if(n==0 || w==0)
      return 0;
   // if last item is over capacity
   if (wt[n-1] > w)
      return knapsack(w, wt, val, n-1);
   // return max( include last item, exclude last item )
   return max( val[n-1] + knapsack(w - wt[n-1], wt, val, n-1)
      , knapsack(w, wt, val, n-1)
   );
}

int main() {
   int w, n, i;
   int *val, *wt;
   scanf("%i %i", &w, &n);
   scanf("%i %i", &w, &n);
   scanf("%i %i", &i, &i);
   val = (int*)malloc(n * sizeof(int));
   wt = (int*)malloc(n * sizeof(int));
   for(i=0; i<n; i++) {
      fprintf(stderr, "Enter #%i ", i);
      scanf("%i %i", &val[i], &wt[i]);
   }
   printf("\nKnapsacking...\n");
   printf("%d", knapsack(w, wt, val, n));
   free(val);
   free(wt);
   return 0;
}



