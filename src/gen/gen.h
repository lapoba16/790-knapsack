/*
 * @file    gen.h
 * @author  Bel LaPointe (lapoba16)
 * @date    2017/02/18
 * @version 1.0
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/* 
 * main
 * 
 * Usage:
 * ./gen #inPop #gens cap #items
 *
 * Each is optional, but order is required.
 *
 * Prints items to stdout
 */
int main(int, char**);
