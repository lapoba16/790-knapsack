/*
 * @file    gen.cpp
 * @author  Bel LaPointe (lapoba16)
 * @date    2017/02/18
 * @version 1.0
 *
 */
#include "gen.h"

int main(int argc, char **argv) {
   int i, v, m;

   srand(time(NULL));

   int pop = (argc>1) ? atoi(argv[1]) : 10;
   int gen = (argc>2) ? atoi(argv[2]) : 20;
   int cap = (argc>3) ? atoi(argv[3]) : 15;
   int cnt = (argc>4) ? atoi(argv[4]) : 10;
   int sel_d = (argc>5) ? atoi(argv[5]) : -1;
   int sel_t = (argc>6) ? atoi(argv[6]) : -1;

   int maxval = 2*cap;
   int maxmas = (cap > cnt) ? sqrt(cnt) : sqrt(cap);

   fprintf(stderr, "\n");
   fprintf(stderr
      , "pop=%i   gen=%i   cap=%i   cnt=%i   sel_d=%i   sel_t=%i   maxval=%i   maxmas=%i\n"
      , pop, gen, cap, cnt, sel_d, sel_t
      , maxval, maxmas
   );
   fprintf(stderr, "\n");

   float portion;
   portion = rand() % 40 + 15;
   portion /= 10;
   // random portion of items to be included; approx 1/portion in ideal solution (very rough)
   // ranges from 1/1.5 (many items included) to 1/5.4 (few items included)
   // arbitrarily chosen
   while((cnt*maxmas)/cap > portion) {
      maxmas /= 2;
   }
   while((cnt*maxmas)/cap <= portion) {
      maxmas *= 2;
      //fprintf(stderr, "MAXMAS = %i\n", maxmas);
   }
   maxmas = (maxmas > 5) ? maxmas : 5;
   maxval = (maxval > 5) ? maxval : 5;
   fprintf(stderr, "%i*%i <= %.1f * %i\n%i < %.1f\n", cnt, maxmas, portion, cap, (cnt*maxmas)/cap, portion);


   printf("%i\n%i\n%i\n%i\n\n%i\n%i\n"
      , pop
      , gen
      , cap
      , cnt
      , sel_d
      , sel_t
   );
   for(i=0; i<cnt; i++) {
      v = rand()%maxval + 1;
      m = rand()%(maxmas*2/3) + maxmas/3;
      fprintf(stdout, "%i %i\n", v, m);
      //fprintf(stderr, "%i %i\n", v, m);
   }

   return 0;
}
