/*
 * @file    mutate.h
 * @author  Bel LaPointe (lapoba16)
 * @date    2017/03/19
 * @version 1.0
 *
 */
#include <stdio.h>
#include <stdlib.h>

#include "common.h"

/*
 * int mutation(bool **a, int b, int c, float d)
 * 
 * Given a population a[b][c] with b players and
 * c features, randomly flip every feature with
 * probability d.
 *
 * @param   a  source
 * @param   b  source cnt
 * @param   c  feature cnt
 * @param   d  mutation rate
 * @return  irrelevant, a is updated
 */
int mutation(bool **, int, int, float);

