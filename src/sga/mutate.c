/*
 * @file    mutate.cpp
 * @author  Bel LaPointe (lapoba16)
 * @date    2017/03/19
 * @version 1.0
 *
 */
#include "mutate.h"

int mutation(bool **cur, int pop, int cnt, float rate) {
   // for each player
   int i, j;
   float r;
   for(i=0; i<pop; i++)
      // for each feature
      for(j=0; j<cnt; j++) {
         r = rand() % 100 / 100.0;
         // if mutation, flip
         cur[i][j] = ( r < rate ) ^ cur[i][j];
      }
   return 0;
}
