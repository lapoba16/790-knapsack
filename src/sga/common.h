#include <stdio.h>

#ifndef BEL_BOOL
#define BEL_BOOL
typedef enum { false, true } bool;
#endif

#ifndef BL_DEMETYPE
#define BL_DEMETYPE
typedef unsigned demeType;
#endif

/*
 * bool sort(int *a, int b)
 *
 * Sort b elements in array a
 *
 * @param   a  items to sort
 * @param   b  how many items in a
 *
 * @return  the sorted list
 */
int* sort(int *, int);

/*
 * int part(int *a, int b)
 *
 * Reorganize a so things less than
 * a[b-1] are on the left side. Return
 * splitting point.
 *
 * @param   a  array to reorganize
 * @param   b  len(a)
 *
 * @return  int   split point
 */
int part(int *, int);


/*
 * int toPercentile(int *a, bool **c, int b)
 *
 * Convert a list of values to their
 * percentiles. This also sorts the list.
 * The items in A are associated with c,
 * retain relationship.
 *
 * @param   a  the list to convert
 * @param   c  associated with the order of a
 * @param   b  len(a)
 *
 * @return  average value in list
 */
int toPercentile(int *, bool **, int);

/*
 * int dsort(int *a, int b, bool **c)
 *
 * Sort the list A of B items, but pair
 * a[i] and c[i] and keep them relevant.
 *
 * @param   a  list to sort
 * @param   b  len(a)
 * @param   c  items associated with a's order
 *
 * @return  int   nothing important
 */
int dsort(int *, int, bool **);

/*
 * int dpart(int *a, int b, bool **c)
 *
 * Reorganize a and co so things less than
 * a[b-1] are on the left side and c is
 * reorganized the same as a. Return the
 * splitting point.
 *
 * @param   a  array to reorganize
 * @param   b  len(a)
 * @param   c  associated with a
 *
 * @return  int   splitting point
 */
int dpart(int *, int, bool **);

/*
 * int binsearch(int *a, int b, int c)
 *
 * Binary search for c in array a[b]. -1
 * if it does not exist. Assume a is sorted.
 *
 * @param   a  array to search
 * @param   b  length of a
 * @param   c  search value
 *
 * @return  int   index of search or -1 if not found
 */
int binsearch(int *, int, int);

/*
 * int binsearchInterval(int *a, int b, int c)
 *
 * Binary search for c in a[b] returning
 * the index of the left-part of the interval
 * c falls under. For instance, if a[i]=5 and
 * a[i+1]=7 and the search is 6, return i.
 *
 * @param   a  array to search
 * @param   b  length of a
 * @param   c  value to search for
 *
 * @return  int   index of left of interval
 */
int binsearchIntervalI(int *, int, int);

/*
 * int binsearchInterval(demeType *a, int b, int c)
 *
 * Binary search for c in a[b] returning
 * the index of the left-part of the interval
 * c falls under. For instance, if a[i]=5 and
 * a[i+1]=7 and the search is 6, return i.
 *
 * @param   a  array to search
 * @param   b  length of a
 * @param   c  value to search for
 *
 * @return  int   index of left of interval
 */
int binsearchInterval(demeType *, int, int);

