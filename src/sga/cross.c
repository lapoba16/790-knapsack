/*
 * @file    cross.cpp
 * @author  Bel LaPointe (lapoba16)
 * @date    2017/03/19
 * @version 1.0
 *
 */
#include "cross.h"


int crossover(bool **cur, int pop, int cnt, float rate) {
   // for each pair of parents
   int i;
   int j;
   for(i=0; i<pop/2; i++) {
      // decide if crossing
      float r = rand() % 100 / 100.0;
      if(r<rate) {
         // decide where second begins
         r = rand() % (cnt-1) + 1;
         //cerr << "   Crossing " << 2*i << " and " << 2*i+1 << " at " << r <<endl;
         // trade before there
         for(j=0; j<r; j++) {
            cur[2*i][j] = cur[2*i][j] ^ cur[2*i+1][j];
            cur[2*i+1][j] = cur[2*i+1][j] ^ cur[2*i][j];
            cur[2*i][j] = cur[2*i][j] ^ cur[2*i+1][j];
         }
      }
   }
   return 0;
}
