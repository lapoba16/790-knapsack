/*
 * @file    fitness.h
 * @author  Bel LaPointe (lapoba16)
 * @date    2017/03/19
 * @version 1.0
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"

/*
 * int fitness(bool **a, int b, int c, int *d, int **e
 * , int f, int *g, int *h, int *i)
 * 
 * Given a bool ** to an array [b][c], compute
 * the fitness for knapsack using *d where |d|
 * =2b, d[2i]=value, d[2i+1]=weight.
 * Return an int* array of length b (one for each
 * in pop) that is fitness for each.
 *
 * @param   a  include/exclude c items for b=|pop|
 * @param   b  number of members of pop
 * @param   c  number of items
 * @param   d  val,weight,val,weight...2c-1
 * @param   e  to avoid re-new'ing fitness storage
 * @param   f  capacity of carry
 * @param   g  index of best member
 * @param   h  index of worst non-1 member
 * @param   i  number of 1 members
 * @return  avg fitness
 */
int fitness(bool **, int, int, int *, int **, int, int *, int *, int *);
