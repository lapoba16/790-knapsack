/*
 * @file    common.cpp
 * @author  Bel LaPointe (lapoba16)
 * @date    2017/03/19
 * @version 1.0
 *
 */
#include "common.h"

int toPercentile(int *a, bool **b, int n) {
   int avg = 0;
   // convert to percentiles, ties round down
   /*********************TIMING**************/
   dsort(a, n, b);
   /*********************TIMING**************/
   int i=0;
   int k;
   while (i < n) {
      int per = ( (float) (i+1) / n ) * 100.0;
      per = (per > 0 ) ? per : 1;
      int j = i;
      while(j+1 < n && a[j+1] == a[i])
         j++;
      for(k=i; k<=j; k++) {
         avg += a[k];
         a[k] = per;
      }
      i = j+1;
   }
   return avg / n;
}



int* sort(int *a, int n) {
   if (1 < n) {
      int p = part(a, n);
      sort(a        , p );
      sort(&(a[p+1]), n-(p+1) );
   }
   return a;
}

int part(int *a, int h) {
   if (h<=1)
      return h;
   int piv = a[h-1];
   int i = 0, j;
   int tmp;
   for(j = 0; j < h; j++) {
      if (a[j] < piv) {
         tmp = a[j];
         a[j] = a[i];
         a[i] = tmp;
         i++;
      }
   }
   i--;
   tmp = a[i+1];
   a[i+1] = a[h-1];
   a[h-1] = tmp;
   return i+1;
}

int dsort(int *a, int n, bool **b) {
   if (1 < n) {
      int p = dpart(a, n, b);
      dsort(a        , p         , b         );
      dsort(&(a[p+1]), n-(p+1)   , &(b[p+1]) );
   }
   return n;
}

int dpart(int *a, int n, bool **b) {
   if (n<=1)
      return n;
   int piv = a[n-1];
   int i = 0, j;
   int tmp;
   bool *btmp;
   for(j = 0; j < n; j++) {
      if (a[j] < piv) {
         tmp = a[j];
         a[j] = a[i];
         a[i] = tmp;
         btmp = b[j];
         b[j] = b[i];
         b[i] = btmp;
         i++;
      }
   }
   i--;
   tmp = a[i+1];
   a[i+1] = a[n-1];
   a[n-1] = tmp;
   btmp = b[i+1];
   b[i+1] = b[n-1];
   b[n-1] = btmp;
   return i+1;
}

int binsearch(int *a, int b, int c) {
   int l = 0, r = b-1;
   while(l <= r) {
      if (a[(l+r)/2] < c) {
         l = (l+r) / 2 + 1;
      } else if (a[(l+r)/2] > c) {
         r = (l+r) / 2 - 1;
      } else {
         return (l+r)/2;
      }
   }
   return -1;
}

int binsearchIntervalI(int *a, int b, int c) {
   int l = 0, r = b-1;
   int cur;
   while(l <= r) {
      cur = (l+r)/2;
      if (   cur==0 || cur==b-1 || ( c < a[cur] && c >= a[cur-1] ) )
         return cur;
      else if (   cur<b && c >= a[cur]  )
         l = cur + 1;
      else //if (   cur>0 && c <  a[cur-1]    )
         r = cur - 1;
   }
   return -2;
}

int binsearchInterval(demeType *a, int b, int c) {
   int l = 0, r = b-1;
   int cur;
   while(l <= r) {
      cur = (l+r)/2;
      if (   cur==0 || cur==b-1 || ( c < a[cur] && c >= a[cur-1] ) )
         return cur;
      else if (   cur<b && c >= a[cur]  )
         l = cur + 1;
      else //if (   cur>0 && c <  a[cur-1]    )
         r = cur - 1;
   }
   return -2;
}

