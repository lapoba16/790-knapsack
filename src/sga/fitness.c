/*
 * @file    fitness.cpp
 * @author  Bel LaPointe (lapoba16)
 * @date    2017/03/19
 * @version 1.0
 *
 */
#include "fitness.h"

int fitness(bool **cur, int pop, int cnt, int *items, int **oldfit, int icap, int *w, int *loser, int *lcnt) {
   float cap = icap;
   int curmax = 0;
   int avg = 0;
   if(*oldfit==NULL || *oldfit==0) {
      *oldfit = (int*)malloc(pop * sizeof(int));
      memset(*oldfit, 0, pop*sizeof(int));
   }
   int *newfit = *oldfit;
   *w = *loser = *lcnt = 0;
   int i, j;
   // for each member of pop
   for(i=0; i<pop; i++) {
      /*******************TIMING**************/
      //newfit[i] = 5;
      //avg += newfit[i];
      //continue;
      /*******************TIMING**************/
      // sum value and weight
      newfit[i] = 0;
      int weight = 0;
      for(j=0; j<cnt; j++) {
         // if player i has item j
         if(cur[i][j]) {
            // val += j's value
            newfit[i] += items[2*j];
            // weight += j's weight
            weight += items[2*j+1];
         }
      }
      // determine fitness
      if(weight > 2*cap)
         newfit[i] = 1;
      else if (weight > cap)
         //newfit[i] *= ((weight-cap) / cap)/2;
         newfit[i] *= ((float)cap / weight) / 2.0;
      else  
         //newfit[i] *= (weight / cap);
         ;
      if(newfit[i] <= 1) {
         newfit[i] = 1;
         *lcnt = *lcnt + 1;
      }
      if (newfit[i] > newfit[*w])
         *w = i;
      if ( ( newfit[i]>1 && newfit[i] < newfit[*loser] ) || newfit[*loser]==1 ) {
         *loser = i;
      }
      avg += newfit[i];
   }
   return avg / pop;
}

