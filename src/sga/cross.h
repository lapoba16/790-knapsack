/*
 * @file    cross.h
 * @author  Bel LaPointe (lapoba16)
 * @date    2017/03/19
 * @version 1.0
 *
 */
#include <stdio.h>
#include <stdlib.h>

#include "common.h"


/*
 * int crossover(bool **a, int b, int c, float d)
 * Given a population a[b][c] with b players and
 * c features, cross [2i] and [2i+1] with probability
 * d. Replace with children.
 *
 * @param   a  source parents
 * @param   b  parent cnt
 * @param   c  parent feature cnt
 * @param   d  crossover rate
 * @return  irrelevant, a is updated
 */
int crossover(bool **, int, int, float);
