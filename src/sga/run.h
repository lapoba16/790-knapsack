#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "common.h"
#include "cross.h"
#include "fitness.h"
#include "mutate.h"
#include "select.h"

int main(int, char **);

/*
 * bool* knapsack(int *a, int b, int c, int d, int e, int f, int g)
 *
 * Given an array of a[2i]=value and a[2i+1]
 * =weight on b items, use a genetic algorithm 
 * to a population size c for d generations and
 * return a boolean array of whether to include
 * the item.
 *
 * @param   a  items w/ value,weight,value,weight...
 * @param   b  len(a)/2 (# of pairs)
 * @param   c  population size
 * @param   d  number of gens
 * @param   e  weight capacity
 * @param   f  deme type for selection (-1 for none, 0 for plus, else radius)
 * @param   g  tournament size for selection (<1 for none, else bracket levels)
 */
bool* knapsack(int *, int, int, int, int, int, int);

/*
 * bool** initPop(int a, int b, int *c)
 *
 * Given a popluation size a and a chromosome
 * size b, randomly create a population size a
 * each with b boolean values. Return as
 * bool[a][b];
 * Use information from c(the items' values and
 * weights) to determine the density of individuals.
 *
 * @param   a  population size
 * @param   b  chromosome size
 * @param   c  items in knapsack length 2b where
 *             [2i]=val and [2i+1]=weight
 *
 * @return  bool[a][b] 2-d dyamic array of bools
 */
bool** initPop(int, int, int *);


/*
 * int report(int *a, bool *b, int c, int d)
 *
 * Print a report for the results
 *
 * @param   a  [2*c], [2i]=item val, [2i+1]=item mass
 * @param   b  c booleans if item is included or not
 * @param   c  total items
 * @param   d  carrying capacity
 * @return  meaningless
 */
int report(int *, bool *, int, int);
