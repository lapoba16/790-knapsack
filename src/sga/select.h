/*
 * @file    select.h
 * @author  Bel LaPointe (lapoba16)
 * @date    2017/03/19
 * @version 1.0
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"

/*
#ifndef BL_DEMETYPE
#define BL_DEMETYPE
typedef int demeType;
#endif
*/

/*
 * int selection (bool **a, int b, int c, int *d, int e, int f)
 *
 * Super-selection method. Do selection for a[b][c] given
 * fitness scores d according to e and f
 * if e < 0
 *    not demes
 * else if e == 0
 *    plus demes
 * else demesz = e
 * else if f < 1
 *    fitness proportional (roulette)
 * else tournament size = f
 */
int selection(bool **, int, int, int *, int, int);

enum SELTYPE_CODE {
   SEL_VAN=-1
   , SEL_PLU=0
   , SEL_RAD=1
   , SEL_ROU=-1
   , SEL_TRN=1
};

/*
 * int Selection(bool **a, int b, int c, int *d)
 *
 * Given a population a[b][c] with b players and
 * c features and their fitnesses d[b], select b
 * parents and reinit pop so parents are together.
 * Select with replacement.
 *
 * EDIT: :
 *
 * @param   a  source parents
 * @param   b  parent cnt
 * @param   c  parent feature cnt
 * @param   d  parent rating
 * @return  irrelevant, a is updated
 */
int Selection(bool **, int, int, int*);

/*
 * int dtSelection(bool **a, int b, int c, int *d, int e, int f)
 *
 * Use (d)emes and (t)ournament selection to
 * select parents for a new generation.
 *
 * Given a population size a[b][c], reorder (with
 * duplicates) so [2i] and [2i+1] are parents
 * to the upcoming [2i] and [2i+1]. Use deme
 * radius e and tournament bracket size f.
 * 
 * @param   a  the parents' details
 * @param   b  the # of parents
 * @param   c  the # of features in each parent
 * @param   d  the score of each parent
 * @param   e  the deme radius
 * @param   f  the tournament bracket size (in levels)
 *
 * @return  int   meaningless
 */
int dtSelection(bool **, int, int, int *, int, int);


/*
 * int drSelection(bool **a, int b, int c, int *d, int e)
 *
 * Use (d)emes and (r)oulette selection to
 * select parents for a new generation.
 *
 * Given a population size a[b][c], reorder (with
 * duplicates) so [2i] and [2i+1] are parents
 * to the upcoming [2i] and [2i+1]. Use deme
 * radius e.
 * 
 * @param   a  the parents' details
 * @param   b  the # of parents
 * @param   c  the # of features in each parent
 * @param   d  the score of each parent
 * @param   e  the deme radius
 *
 * @return  int   meaningless
 */
int drSelection(bool **, int, int, int *, int);

/*
 * int prSelection(bool **a, int b, int c, int *d)
 *
 * Use (p)lus demes and (r)oulette selection to
 * select parents for a new generation.
 *
 * Given a population size a[b][c], reorder (with
 * duplicates) so [2i] and [2i+1] are parents
 * to the upcoming [2i] and [2i+1]. Use deme
 * radius e.
 * 
 * @param   a  the parents' details
 * @param   b  the # of parents
 * @param   c  the # of features in each parent
 * @param   d  the score of each parent
 *
 * @return  int   meaningless
 */
int prSelection(bool **, int, int, int *);

/*
 * int ptSelection(bool **a, int b, int c, int *d, int f)
 *
 * Use (p)lus demes and (t)ournament selection to
 * select parents for a new generation.
 *
 * Given a population size a[b][c], reorder (with
 * duplicates) so [2i] and [2i+1] are parents
 * to the upcoming [2i] and [2i+1]. Use deme
 * radius e and tournament bracket size f.
 * 
 * @param   a  the parents' details
 * @param   b  the # of parents
 * @param   c  the # of features in each parent
 * @param   d  the score of each parent
 * @param   f  the tournament bracket size (in levels)
 *
 * @return  int   meaningless
 */
int ptSelection(bool **, int, int, int *, int);

/*
 * int zlookup(int a, int b)
 *
 * Lookup the index of a's neighbor in direction
 * b based on a z-morton layout. b translates
 * to
 *
 * 0  =  up
 * 1  =  down
 * 2  =  left
 * 3  =  right
 *
 * @param   a  from index
 * @param   b  direction
 *
 * @return  int   index of neighbor in some direction
 */
int zlookup(int , int );

/*
 * int zrowfill(int a, int b, demeType *c
 *
 * Given an allocated array c[], get the z-morton
 * neighbors left-right of ID a to fill a row
 * b long.
 *
 * @param   a  center of row
 * @param   b  length of row
 * @param   c  fill row IDs in here
 *
 * @return  int   the number of values added to c
 */
int zrowfill(int, int, demeType *);

/*
 * int zedgefill(int a, int b, demeType *c)
 *
 * Given an allocated array c[], get the z-morton
 * neighbors left-right of ID a at a distance
 * of b. (To get the circle of neighbors, the
 * top-bottom rows are totally filled. The
 * left-right edges are done here.
 *
 * @param   a  center of search
 * @param   b  width to search
 * @param   c  array to fill
 *
 * @return  int   number of items added
 */
int zedgefill(int , int , demeType *);

enum ZDIR_CODE {
   ZDIR_UP=0
   , ZDIR_DOWN=1
   , ZDIR_LEFT=2
   , ZDIR_RIGHT=3
};

/*
 * int getRadiusDeme(int a, int b, demeType**c, int d)
 *
 * Given a source node id A and a z-morton
 * population, get the node IDs of a's neighbors
 * within b blocks. Assume diagonal=1 block.
 * Do not meet or exceed id d.
 *
 * For example, if my source a=3 and dist b=1,
 * my z-morton deme is 3,0,1,4,2,6,8,9,12 (3x3
 * centered on 3.
 *
 * @param   a  center ID
 * @param   b  dist to neighbors
 * @param   c  will become a dynamic array of deme members' IDs
 *             MUST BE FREED. Must be null if not alloc'd.
 * @param   d  # of individuals in z-morton
 *
 * @return  int   # of deme members
 */
int getRadiusDeme(int, int, demeType **, int);

/*
 * getPlusDeme(int a, demeType **b, int c)
 *
 * Given a source node id a and a z-morton
 * population (or a linear one we can consider
 * z-morton), get the node IDs of a's neighbors
 * in a plus-pattern. Do not meet or exceed
 * id c.
 *
 * @param   a  center ID
 * @param   b  will become a dynamic array of deme members' ids
 *             MUST BE FREED, must be null or 0 if not alloc'd
 * @param   c  # of individuals in population
 *
 * @return  int   # of deme members
 */
int getPlusDeme(int, demeType **, int);


/*
 * int doTournament(demeType *a, int b, int c, demeType *d)
 *
 * Do a tournament between up to 2**c members 
 * of a[b] using their scores in d[].
 *
 * @param   a  tournament contestant IDs in d
 * @param   b  # of contestants
 * @param   c  tournament levels
 * @param   d  scores of a[]
 *
 * @return  int   the winner's VALUE, not their index
 */
int doTournament(demeType *, int, int, demeType*);


/*
 * int doRoulette(demeType *a, int b, int *c)
 *
 * Given a list of IDs a[b], do roulette
 * selection given their fitness in c[]
 *
 * @param   a  IDs to consider
 * @param   b  len of a
 * @param   c  compounding summed fitness
 *
 * @return  int   chosen ID
 */
int doRoulette(demeType *, int, int *);
