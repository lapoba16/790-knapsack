#include "run.h"

int main(int argc, char ** argv) {
   int pop, gen, cap, cnt, sel_d, sel_t;
   int i;
   fprintf(stderr, "How many in population? ");
   scanf("%i", &pop);
   fprintf(stderr, "How many generations? ");
   scanf("%i", &gen);
   fprintf(stderr, "What capacity? ");
   scanf("%i", &cap);
   fprintf(stderr, "How many items? ");
   scanf("%i", &cnt);
   fprintf(stderr, "Would you like demes? -1 for no, 0 for plus-shaped, >0 for radius.\n");
   scanf("%i", &sel_d);
   fprintf(stderr, "Would you like tournaments? <1 for no, >0 for levels.\n");
   scanf("%i", &sel_t);
   fprintf(stderr, "Please enter %i lines with the format \"value weight\".\n", cnt);
   int *items = (int*) malloc (2 * cnt * sizeof(int));
   for(i=0; i<cnt; i++) {
      scanf("%i %i", &(items[2*i]), &(items[2*i+1]));
   }
   fprintf(stderr, "Knapsacking...\n");
   bool *computed = knapsack(items, cnt, pop, gen, cap, sel_d, sel_t);
   //report(items, computed, cnt, cap);
   free(items);
   free(computed);
   return 0;
}

void print2dboolwithindex(bool **in, int a, int b, int *d) {
   int i, j;
   for(i=0; i<a; i++) {
      printf("%3i   %3i   ", i, d[i]);
      for(j=0; j<b; j++)
         printf("%i ", in[i][j]);
      printf("\n");
   }
}
void print2dbool(bool **in, int a, int b) {
   int i, j;
   for(i=0; i<a; i++) {
      printf("%3i   ", i);
      for(j=0; j<b; j++)
         printf("%i ", in[i][j]);
      printf("\n");
   }
}

bool* knapsack(int *items, int cnt, int pop, int gen, int cap, int sel_d, int sel_t) {
   float cross = .65;
   float mutate = .02;
   srand(time(NULL));
   // create initial population |pop|
   bool **cur = initPop(pop, cnt, items);
   // fitness of population
   int *fit = NULL;
   int *trash = NULL;
   int leader = 0;
   int loser, lcnt=0;
   int i, j;

   int avg = fitness(cur, pop, cnt, items, &fit, cap, &leader, &loser, &lcnt);
   if(sel_d < 0)
      toPercentile(fit, cur, pop);

   int bestg = 0;
   int best = 0;

   //print2dboolwithindex(cur, pop, cnt, fit);
   fprintf(stderr, "pop=%i   gen=%i   cap=%i   cnt=%i   sel_d=%i   sel_t=%i\n"
      , pop, gen, cap, cnt, sel_d, sel_t
   );

   // for gen generations
   fprintf(stdout, "%i\n", gen);
   for(i=0; i<gen; i++) {
      fprintf(stderr, "\r");
      for(j=0; j<((float)i/gen)*100; j++)
         fprintf(stderr,".");
      fflush(stderr);
      fprintf(stdout, "%3i %3i\n", i, avg);
      //print2dboolwithindex(cur, pop, cnt, fit);
      // GA stuff
      selection(cur, pop, cnt, fit, sel_d, sel_t);
      crossover(cur, pop, cnt, cross);
      mutation(cur, pop, cnt, mutate);
      avg = fitness(cur, pop, cnt, items, &fit, cap, &leader, &loser, &lcnt);

      if(i==gen-1 && cnt < 20)
         for(j=0; j<pop; j++)
            fprintf(stderr, "   [%i] = %i\n", j, fit[j]);

      // tried using percentile instead of fitness value for selection. Not great.
      // later restored using percentile, works about the same or better
      if(sel_d < 0) {
         toPercentile(fit, cur, pop);
         leader = pop-1;
      } else {
         for(j=0; j<pop; j++)
            leader = ( fit[leader] > fit[j] ) ? leader : j;
      }
      // /GA stuff
      
      // update global best
      int o = 0;
      int k = fitness(&(cur[leader]), 1, cnt, items, &trash, cap, &o, &o, &o);

      if (k > best) {
         best = k;
         bestg = i;
      }
      // print worst non-1 and count of 1's fitnesses
      fprintf(stdout, "%i %i\n", loser, lcnt);
      // print best for plot.py
      fprintf(stdout, "%i %i\n", best, bestg);
      if(i==gen-1)
         fprintf(stderr, "\nbest = %i   bestg = %i\n", best, bestg);
      for(j=0; j<cnt; j++)
         fprintf(stdout, "%i ", cur[leader][j]);
      fprintf(stdout, "%i ", k);


      fprintf(stdout, "\n");

   }
   fprintf(stderr, "\n          SGA DONE\n");
   // stdout items and val for graphing
   for(i=0; i<cnt; i++)
      fprintf(stdout, "%i ", items[2*i]);
   fprintf(stdout, "\n");
   for(i=0; i<cnt; i++)
      fprintf(stdout, "%i ", items[2*i+1]);
   fprintf(stdout, "\n");
   // save best
   int max = 0;
   for(i=1; i<pop; i++)
      max = (fit[i] > fit[max]) ? i : max;
   // free all
   for(i=0; i<pop; i++)
      if(i!=max)
         free(cur[i]);
   bool *winner= cur[max];
   free(cur);
   free(fit);
   free(trash);
   // return highest fitness as solution
   return winner;
}

bool** initPop(int pop, int cnt, int *items) {
   int n = 0;
   int i, j;
   n = 0;
   // get avg weight
   for(i=0; i<cnt; i++)
      n += items[2*i+1];
   n /= cnt;
   n = 3;
   n = 10;
   // [pop][cnt]
   bool **cur = (bool**) malloc (pop * sizeof(bool*));
   // all to random value
   // 1/n chance of including item
   for(i=0; i<pop; i++) {
      cur[i] = (bool*) malloc (cnt * sizeof(bool));
      memset(cur[i], 0, cnt*sizeof(bool));
      for(j=0; j<cnt; j++)
         cur[i][j] = (rand() % n) == 0;
   }
   return cur;
}


int report(int *items, bool *winner, int cnt, int cap) {
   int *score = NULL;
   int w, i, l;
   char format[] = "\n%10s:   ";
   fitness(&winner, 1, cnt, items, &score, cap, &w, &l, &l);
   printf("WINNER: %i\n", *score);
   printf(format, "ITEM_VAL");
   for(i=0; i<cnt; i++)
      printf("%5i ", items[2*i]);
   printf(format, "ITEM_MASS");
   for(i=0; i<cnt; i++)
      printf("%5i ", items[2*i+1]);
   printf(format, "ITEM_DENSE");
   for(i=0; i<cnt; i++)
      printf("%5.1f ", (float)items[2*i]/items[2*i+1]);
   printf(format, "ITEM_TAKE");
   for(i=0; i<cnt; i++)
      printf("%5i ", winner[i]);
   printf("\n");
   free(score);
   return 0;
}




