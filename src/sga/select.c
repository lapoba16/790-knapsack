/*
 * @file    select.cpp
 * @author  Bel LaPointe (lapoba16)
 * @date    2017/03/19
 * @version 1.0
 *
 */
#include "select.h"

int selection(bool **cur, int pop, int cnt, int *fit, int demetrig, int tournTrigger) {
   if( demetrig < 0 ) {
      //fprintf(stderr, "vanilla\n");
      // no demes, no tournament/forced roulette
      return Selection(cur, pop, cnt, fit);
   }
   else if ( demetrig == 0 ) {
      // plus demes
      if ( tournTrigger >= 1 ) {
         //fprintf(stderr, "plus tourn\n");
         // tournaments + plus = ptSelection
         return ptSelection(cur, pop, cnt, fit, tournTrigger);
      }
      else {
         //fprintf(stderr, "plus roul\n");
         // roulette + plus = prSelection
         return prSelection(cur, pop, cnt, fit);
      }
   } else {
      // radius demes
      if ( tournTrigger >= 1 ) {
         //fprintf(stderr, "deme tourn\n");
         // tournaments + demes = dtSelection
         return dtSelection(cur, pop, cnt, fit, demetrig, tournTrigger);
      }
      else {
         //fprintf(stderr, "deme roul\n");
         // roulette + demes = drSelection
         return drSelection(cur, pop, cnt, fit, demetrig);
      }
   }
}

int Selection(bool **cur, int pop, int cnt, int *fit) {
   bool **next = (bool**) malloc( pop * sizeof(bool*));
   int compound[pop];
   int i;
   compound[0] = fit[0];
   for(i=1; i<pop; i++) {
      compound[i] = fit[i] + compound[i-1];
   }
   // select pop new parents
   for(i=0; i<pop; i++) {
      next[i] = (bool*) malloc (cnt * sizeof(bool));
      // if random value falls into j's fitness span
      int r = rand() % compound[pop-1];
      int j = binsearchIntervalI(compound, pop, r);
      // j is chosen as i'th parent
      memcpy(next[i], cur[j], sizeof(bool)*cnt);
   }
   for(i=0; i<pop; i++) {
      free(cur[i]);
      cur[i] = next[i];
   }
   free(next);
   return 0;
}

int prSelection(bool **cur, int pop, int cnt, int *fit) {
   demeType *deme = 0, demelen;
   bool **next = (bool**)malloc(pop * sizeof(bool*));
   int i = 0, j=0, k;
   // for each position
   for(i=0; i<pop; i++) {
      next[i] = (bool*) malloc(cnt * sizeof(bool));
      // get deme members' ids
      demelen = getPlusDeme(i, &deme, pop);
      // select 1 as parent
      j = doRoulette(deme, demelen, fit);
      memcpy(next[i], cur[j], sizeof(bool) * cnt);
   }
   for(i=0; i<pop; i++) {
      free(cur[i]);
      cur[i] = next[i];
   }
   free(next);
   free(deme);
   return 0;
}

int ptSelection(bool **cur, int pop, int cnt, int *fit, int tsize) {
   demeType *deme = 0, demelen;
   bool **next = (bool**)malloc(pop * sizeof(bool*));
   demeType nfit[pop];
   int i = 0, j=0, k;
   for(i=0; i<pop; i++)
      nfit[i] = fit[i];
   // for each position
   for(i=0; i<pop; i++) {
      next[i] = (bool*) malloc(cnt * sizeof(bool));
      // get deme members' ids
      demelen = getPlusDeme(i, &deme, pop);
      // select 1 as parent
      j = doTournament(deme, demelen, tsize, nfit);
      memcpy(next[i], cur[j], sizeof(bool) * cnt);
   }
   for(i=0; i<pop; i++) {
      free(cur[i]);
      cur[i] = next[i];
   }
   free(next);
   free(deme);
   return 0;
}

int drSelection(bool **cur, int pop, int cnt, int *fit, int dsz) {
   demeType *deme = 0, demelen;
   bool **next = (bool**)malloc(pop * sizeof(bool*));
   int i = 0, j=0, k;
   // for each position
   for(i=0; i<pop; i++) {
      next[i] = (bool*) malloc(cnt * sizeof(bool));
      // get deme members' ids
      demelen = getRadiusDeme(i, dsz, &deme, pop);
      // select 1 as parent
      j = doRoulette(deme, demelen, fit);
      memcpy(next[i], cur[j], sizeof(bool) * cnt);
   }
   for(i=0; i<pop; i++) {
      free(cur[i]);
      cur[i] = next[i];
   }
   free(next);
   free(deme);
   return 0;
}

int doRoulette(demeType *a, int n, int *fit) {
   /*
   int i;
   int r = rand() % fit[n-1];
   return a[binsearchInterval(fit, n, r)];
   */
   demeType i, r;
   demeType afit[n];
   // sort in hopes of avoiding some jumps in memory
   //sort(a, n);
   // get fitness of each
   afit[0] = fit[a[0]];
   for(i=1; i<n; i++) {
      afit[i] = fit[a[i]] + afit[i-1];
   }
   // roulette
   r = rand() % afit[n-1];
   return a[binsearchInterval(afit, n, r)];
   /*
   demeType ttl = 0, i, r;
   demeType afit[n];
   // sort in hopes of avoiding some jumps in memory
   //sort(a, n);
   // get fitness of each
   for(i=0; i<n; i++) {
      afit[i] = fit[a[i]];
      ttl += afit[i];
   }
   // roulette
   r = rand() % ttl;
   i = 0;
   ttl = i = 0;
   while(ttl <= r) {
      ttl += afit[i];
      i++;
   }
   return a[i-1];
   */
}

int dtSelection(bool **cur, int pop, int cnt, int *fit, int dsz, int trnsz) {
   demeType *deme = 0, demelen;
   bool **next = (bool**)malloc(pop * sizeof(bool*));
   int i = 0, j=0, k;
   demeType nfit[pop];
   for(i=0; i<pop; i++)
      nfit[i] = fit[i];
   // for each position
   for(i=0; i<pop; i++) {
      next[i] = (bool*) malloc(cnt * sizeof(bool));
      // get deme members' ids
      demelen = getRadiusDeme(i, dsz, &deme, pop);
      // select 1 as parent
      j = doTournament(deme, demelen, trnsz, nfit);
      memcpy(next[i], cur[j], sizeof(bool) * cnt);
   }
   for(i=0; i<pop; i++) {
      free(cur[i]);
      cur[i] = next[i];
   }
   free(next);
   free(deme);
   return 0;
}

int doTournament(demeType *grp, int sz, int trnsz, demeType *fit) {
   if(grp==NULL || grp==0)
      return -1;
   // get 2**trnsz unique members of grp
   int len = 1, i, j;
   for(i=0; i<trnsz; i++)
      len *= 2;
   int contestant[len];
   for(i=0; i<len && i<sz; i++) {
      // while exists in array, gen new random id
      // exit if not enough members in grp to fill tournament
      do {
         contestant[i] = rand() % sz;
      } while ( binsearch(contestant, i, contestant[i]) != -1);
      sort(contestant, i+1);
   }
   if(i%2)
      contestant[i++] = contestant[0];
   len = i;
   // do a tournament with len members, which may not be 2**trnsz
   for(i=0; i<len; i++)
      contestant[0] = ( contestant[i] > contestant[0] ) ? contestant[i] : contestant[0] ;
   /*
   while(len > 1) {
      for(i=0; i<len/2; i++)
         if( fit[contestant[i]] < fit[contestant[len-1-i]] )
            contestant[i] = contestant[len-1-i];
      len = (len+1) / 2;
   }
   */
   return contestant[0];
}

int getPlusDeme(int id, demeType **arr, int cap) {
   if(*arr==NULL || *arr==0)
      *arr = (demeType*) malloc  (       sizeof(demeType) * 12 );
   else
      *arr = (demeType*) realloc ( *arr, sizeof(demeType) * 12 );
   demeType *q = *arr;
   // get within 4-block
   int i, j = id - (id%4), z = 0;
   for(i=0; i<4; i++)
      q[z++] = i+j;
   // get above [1] lower row
   q[z++] = zlookup( q[0], ZDIR_UP );
   q[z++] = zlookup( q[1], ZDIR_UP );
   // get right [1] left col
   q[z++] = zlookup( q[1], ZDIR_RIGHT );
   q[z++] = zlookup( q[3], ZDIR_RIGHT );
   // get bottom [2] upper row
   q[z++] = zlookup( q[2], ZDIR_DOWN );
   q[z++] = zlookup( q[3], ZDIR_DOWN );
   // get left [2] right col
   q[z++] = zlookup( q[0], ZDIR_LEFT );
   q[z++] = zlookup( q[2], ZDIR_LEFT );
   // drop if invalid id
   i = 0;
   while (i < z ) {
      if( q[i] >= cap || q[i] < 0 )
         q[i] = q[--z];
      else {
         i++;
      }
   }
   return z;
}

int getRadiusDeme(int id, int dsz, demeType **arr, int cap) {
   if(*arr==NULL || *arr==0)
      *arr = (demeType*) malloc( sizeof(int) * (1 + 2*dsz) * (1 + 2*dsz) );
   else
      *arr = (demeType*) realloc ( *arr, sizeof(int) * (1 + 2*dsz) * (1 + 2*dsz) );
   demeType *q = *arr;
   q[0] = id;
   int i, j, k, z=1, cur;
   // for each circle away from id
   for(i=0; i<dsz; i++) {
      // get top row
      cur = id;
      k = 1 + 2 * (i+1); 
      for(j=0; j<i+1; j++) {
         z += zedgefill(cur, i+1, &(q[z]));
         cur = zlookup(cur, ZDIR_UP);
         // get left-right edges at this height
      }
      z += zrowfill(cur, k, &(q[z]));
      // get bottom row
      cur = id;
      for(j=0; j<i+1; j++) {
         // get left-right edges at this height
         // skip first, got that when we went up
         if( j > 0 )
            z += zedgefill(cur, i+1, &(q[z]));
         cur = zlookup(cur, ZDIR_DOWN);
      }
      z += zrowfill(cur, k, &(q[z]));
   }
   //if( (1+2*dsz) * (1+2*dsz) > z )
   //   *arr = (int*) realloc( *arr, sizeof(int) * z );

   // drop if invalid id
   i = 0;
   while (i < z ) {
      if( q[i] >= cap || q[i] < 0 )
         q[i] = q[--z];
      else
         i++;
   }
   return z;
}

int zrowfill(int center, int width, demeType *fill) {
   if(center < 0)
      return 0;
   int j;
   int z = 0;
   // center done
   fill[z++] = center;
   // left of center
   if(width/2 > 0)
      fill[z++] = zlookup(center, ZDIR_LEFT);
   for(j=1; j<width/2; j++, z++)
      fill[z] = zlookup(fill[z-1], ZDIR_LEFT);
   // right of center
   if(width/2 > 0)
      fill[z++] = zlookup(center, ZDIR_RIGHT);
   for(j=1; j<width/2; j++, z++)
      fill[z] = zlookup(fill[z-1], ZDIR_RIGHT);
   return z;
}

int zedgefill(int center, int width, demeType *fill) {
   if(center < 0)
      return 0;
   int m, l, n;   
   int z = 0;
   m = zlookup(center, ZDIR_LEFT);
   for(l=1; l<width; l++)
      m = zlookup(m, ZDIR_LEFT);
   fill[z++] = m;
   m = zlookup(center, ZDIR_RIGHT);
   for(l=1; l<width; l++)
      m = zlookup(m, ZDIR_RIGHT);
   fill[z++] = m;
   return z;
}

int zlookup(int from, int dir) {
   if(from < 0)
      return -1;
   // skip a bit if up/down, get last bit if left/right
   int carry = 1;
   int sh = 0;
   if (dir < 2) {
      sh++;
   } else {
      dir = dir & 1;
   }
   dir = (dir > 0) ? 1 : -1;
   // if top/left and overflowing
   carry = sh;
   if(dir == -1) {
      while( ((from>>carry)>0) && ((from>>carry)&1) == 0 )
         carry += 2;
      if(from >> carry == 0)
         return -1;
   }
   // else determine target id by bit flips
   carry = 1;
   while(carry) {
      if( (from>>sh)==0 )
         carry = 0;
      else if (dir==1)
         carry = ( ( (from>>sh)&1 ) == 1 );
      else
         carry = ( ( (from>>sh)&1 ) == 0 );
      from ^= 1 << sh;
      sh += 2;
   }
   return from;
}

