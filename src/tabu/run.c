#include "run.h"

int main(int argc, char ** argv) {
   srand(time(NULL));
   int gen, cap, cnt;
   int smem = 5;
   int i;
   fprintf(stderr, "How many generations? ");
   scanf("%i", &gen);
   fprintf(stderr, "What capacity? ");
   scanf("%i", &cap);
   fprintf(stderr, "How many items? ");
   scanf("%i", &cnt);
   fprintf(stderr, "How long is short-term memory? ");
   scanf("%i", &smem);
   fprintf(stderr, "Please enter %i lines with the format \"value weight\".\n", cnt);
   int *items = (int*) malloc (2 * cnt * sizeof(int));
   for(i=0; i<cnt; i++) {
      scanf("%i %i", &(items[2*i]), &(items[2*i+1]));
   }
   bool *computed = knapsack(items, cnt, gen, cap, smem);
   //report(items, computed, cnt, cap);
   free(items);
   free(computed);
   return 0;
}

void print2dboolwithindex(bool **in, int a, int b, int *d) {
   int i, j;
   for(i=0; i<a; i++) {
      printf("%3i   %3i   ", i, d[i]);
      for(j=0; j<b; j++)
         printf("%i ", in[i][j]);
      printf("\n");
   }
}
void print2dbool(bool **in, int a, int b) {
   int i, j;
   for(i=0; i<a; i++) {
      printf("%3i   ", i);
      for(j=0; j<b; j++)
         printf("%i ", in[i][j]);
      printf("\n");
   }
}

bool* knapsack(int *items, int cnt, int gen, int cap, int smem) {
   float cross = .65;
   float mutate = .02;
   // create initial population |pop|
   bool *cur = initPop(cnt);
   bool **his = (bool**) malloc (sizeof(bool*) * smem);
   // fitness of population
   int *fit = NULL;
   int *trash = NULL;
   int leader = 0;
   int loser, lcnt=0;
   int i, j;

   int avg = fitness(&cur, 1, cnt, items, &fit, cap, &leader, &loser, &lcnt);

   int bestg = 0;
   int best = 0;

   //print2dboolwithindex(cur, pop, cnt, fit);
   fprintf(stderr, "gen=%i   cap=%i   cnt=%i\n"
      , gen, cap, cnt
   );
   
   // setup history
   for(i=0; i<smem; i++)
      his[i] = (bool*) malloc (sizeof(bool) * cnt);

   // for gen generations
   fprintf(stdout, "%i\n", gen);
   for(i=0; i<gen; i++) {
      //fprintf(stderr, "\r");
      //for(j=0; j<((float)i/gen)*100; j++)
      //   fprintf(stderr,".");
      //fflush(stderr);
      // TABU SEARCH stuff
      j = tabuSearch(cur, cnt, i, his, smem, gen, items, cap);
      avg = fitness(&cur, 1, cnt, items, &fit, cap, &leader, &loser, &lcnt);
      fprintf(stdout, "%3i %3i\n", i, avg);

      //fprintf(stderr, "   [%i] = %i\n", 0, fit[0]);

      // /TABU SEARCH stuff
      
      // update global best
      if (avg > best) {
         best = avg;
         bestg = i;
      }
      loser = avg;

      // print worst non-1 and count of 1's fitnesses
      fprintf(stdout, "%i %i\n", loser, lcnt);
      // print best for plot.py
      fprintf(stdout, "%i %i\n", best, bestg);
      if(i==gen-1)
         fprintf(stderr, "\nbest = %i   bestg = %i\n", best, bestg);
      for(j=0; j<cnt; j++)
         fprintf(stdout, "%i ", cur[j]);
      fprintf(stdout, "%i ", avg);


      fprintf(stdout, "\n");

   }
   fprintf(stderr, "\n          TABU DONE\n");
   // stdout items and val for graphing
   for(i=0; i<cnt; i++)
      fprintf(stdout, "%i ", items[2*i]);
   fprintf(stdout, "\n");
   for(i=0; i<cnt; i++)
      fprintf(stdout, "%i ", items[2*i+1]);
   fprintf(stdout, "\n");
   // free all
   bool *winner= cur;
   free(fit);
   free(trash);
   for(i=0; i<smem; i++)
      free(his[i]);
   free(his);
   // return highest fitness as solution
   return winner;
}

bool* initPop(int cnt) {
   int i, j;
   // [pop][cnt]
   bool *cur = (bool*) malloc (cnt * sizeof(bool));
   memset(cur, 0, cnt*sizeof(bool));
   // all to random value
   // 1/3 chance of including item
   for(i=0; i<cnt; i++) {
      cur[i] = (rand() % 3) == 0;
   }
   return cur;
}


int report(int *items, bool *winner, int cnt, int cap) {
   int *score = NULL;
   int w, i, l;
   char format[] = "\n%10s:   ";
   fitness(&winner, 1, cnt, items, &score, cap, &w, &l, &l);
   printf("WINNER: %i\n", *score);
   printf(format, "ITEM_VAL");
   for(i=0; i<cnt; i++)
      printf("%5i ", items[2*i]);
   printf(format, "ITEM_MASS");
   for(i=0; i<cnt; i++)
      printf("%5i ", items[2*i+1]);
   printf(format, "ITEM_DENSE");
   for(i=0; i<cnt; i++)
      printf("%5.1f ", (float)items[2*i]/items[2*i+1]);
   printf(format, "ITEM_TAKE");
   for(i=0; i<cnt; i++)
      printf("%5i ", winner[i]);
   printf("\n");
   free(score);
   return 0;
}




