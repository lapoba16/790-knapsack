/*
 * @file    tabu.h
 * @author  Bel LaPointe (lapoba16)
 * @date    2017/03/31
 * @version 1.0
 *
 */
#ifndef BL_TABU_H
#define BL_TABU_H

#include <stdio.h>
#include "common.h"
#include "fitness.h"

#ifndef BL_ONESUBLEN
#define BL_ONESUBLEN
#define ONESUBLEN 8
#endif

#ifndef BL_TABU_HIST_STRUCT
#define BL_TABU_HIST_STRUCT
typedef struct tabu_hist_sig {
   unsigned char sig;
   char freq;
   int density;
} hist_sig;
#endif

/*
 * int tabuSearch(bool *a, int b, int c, bool **d, int e, int f, int *g, int h)
 *
 * Given some individual a of length b
 * at time c, tabu search for the next
 * state. Use history in d of length e.
 *
 * @param   a  current state
 * @param   b  features in state
 * @param   c  time point
 * @param   d  historic states
 * @param   e  # of historic states
 * @param   f  total time points
 * @param   g  items [2i]=val, [2i+1]=weight
 * @param   h  weight capacity
 *
 * @return  fitness change from new individual
 *          from old
 */
int tabuSearch(bool *, int, int, bool **, int, int, int *, int);

/*
 * bool compareState(bool *a, bool *b, int n)
 *
 * Compare two bool arrays of length
 * n and return true if they match.
 *
 * @param   a  first to compare
 * @param   b  second to compare
 * @param   n  length of a,b
 *
 * @return true if they're equal
 */
bool compareState(bool *, bool *, int);

/*
 * int substrToSig(bool *a, int b, hist_sig *c, int *d)
 *
 * For every ONESUBLEN bits in a, create one char value
 * and store it in c. c should be at least
 * length b/ONESUBLEN rounded up. Items stored in d.
 *
 * @param   a  bit string to compress
 * @param   b  length of a
 * @param   c  place to store results, else malloc'd if null
 * @param   d  items in knapsack problem
 *
 * @return  meaningless
 */
int substrToSig(bool *, int, hist_sig *, int *);

/*
 * mediumTermJump(bool *a, int b, hist_sig *c, hist_sig **d, int e, int *f)
 *
 * Given the current string a of length b and its
 * components in c, determine if components of c
 * should be replaced with historic results from d.
 *
 * @param   a  current bit string
 * @param   b  length of a
 * @param   c  components of a, length b/ONSUBLEN + ((b%ONESUBLEN) > 0)
 * @param   d  all historic components, length [|c|][2**ONESUBLEN]
 * @param   e  index of b flipped by tabu search
 * @param   f  items for knapsack yada yada
 *
 * @return bool true if jump was made
 */
bool mediumTermJump(bool *, int, hist_sig *, hist_sig **, int, int *);

/*
 * longTermJump(bool *a, int b, hist_sig *c, hist_sig **d, int *e)
 *
 * Given the current string a of length b and its
 * components in c, determine if components of c
 * should be replaced with historic results from d.
 *
 * @param   a  current bit string
 * @param   b  length of a
 * @param   c  components of a, length b/ONSUBLEN + ((b%ONESUBLEN) > 0)
 * @param   d  all historic components, length [|c|][2**ONESUBLEN]
 * @param   e  items of knapsack problem of length 2b where [2i]=val and [2i+1]=weight
 *
 * @return bool true if jump was made
 */
bool longTermJump(bool *, int, hist_sig *, hist_sig **, int*);

/*
 * sigToSubstr(bool *a, int b, hist_sig *c, int *d, int e)
 *
 * Given a bit string a of length b, convert so its
 * first ONESUBLEN bits match c->sig. Calculate c's
 * stats from items in d (length 2b, where [2i]=val and [2i+1]=weight
 * of item i).
 *
 * @param   a  bit string to update
 * @param   b  length of a
 * @param   c  hist_sig to use as template
 * @param   d  item information from knapsack
 * @param   e  starting position in a
 *
 * @return  int   meaningless
 */
int sigToSubstr(bool *, int, hist_sig *, int *, int);

#endif
