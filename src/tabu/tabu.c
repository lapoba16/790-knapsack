/*
 * @file    tabu.c
 * @author  Bel LaPointe (lapoba16)
 * @date    2017/03/31
 * @version 1.0
 *
 */
#include "tabu.h"

hist_sig **all_hist;
#ifndef BL_TABU_TOLERANCE
#define BL_TABU_TOLERANCE
#define tol_start 5
int tolerance = tol_start;
#endif

int tabuSearch(bool *cur, int len, int t, bool **his, int n, int gen, int *items, int cap) {
   // choose a neighbor of cur not in his
   int pos;
   int *nada = 0;
   int i, j;
   int cfit = fitness(&cur, 1, len, items, &nada, cap, &pos, &pos, &pos);
   int nfit;
   int bpos=-1, bfit=0;
   // find best neighbor not in memory
   for(i = 0; i<len; i++) {
      cur[i] ^= 1;
      // is in memory?
      for(j=0; j<n && j<t; j++) {
         if(compareState(cur, his[j], len))
            break;
      }
      // is better than best?
      if(j==n || (j<n && j==t)) {
         nfit = fitness(&cur, 1, len, items, &nada, cap, &pos, &pos, &pos);
         if(nfit > bfit) {
            bfit = nfit;
            bpos = i;
         }
      }
      // revert change and continue
      cur[i] ^= 1;
   }
   // if all neighbors in history
   if(bpos==-1) {
      fprintf(stderr,"\n!!!No available moves!!!\n");
      // flip random number at random positions
      cfit = rand() % len;
      for(i=0; i<cfit; i++)
         cur[rand()%len] ^= 1;
   }
   else
      cur[bpos] ^= 1;

   // get substrs from chosen cur
   int c = len/ONESUBLEN + ( (len%ONESUBLEN) > 0);
   hist_sig out[c];
   if(all_hist == 0 || all_hist == NULL) {
      all_hist = (hist_sig**) malloc (c * sizeof(hist_sig*));
      i = 1;
      for(j=0; j<ONESUBLEN; j++)
         i *= 2;
      for(j=0; j<c; j++) {
         all_hist[j] = (hist_sig*) malloc (sizeof(hist_sig) * i);
         memset(all_hist[j], 0, sizeof(hist_sig) * i);
      }
   }

   substrToSig(cur, len, out, items);
   // if the move makes things worse,
   // update tolerance
   if(bfit <= cfit || bfit < 2) {
      tolerance --;
      // consider medium term memory
      if(bfit < 2 || tolerance < 1 || !mediumTermJump(cur, len, out, all_hist, bpos, items ) ) {
         if(bfit < 2 || tolerance < 1) {
            // else long term memory and reset tolerance
            //fprintf(stderr, "======== %2i DOING LONG JUMP ========\n", tolerance);
            longTermJump(cur, len, out, all_hist, items);
            tolerance = tol_start;
         }
         else {
            tolerance --;
            //fprintf(stderr, "======== %2i NO JUMP BUT TOLERATING ========\n", tolerance);
         }
      } else {
         //fprintf(stderr, "======== %2i DOING MEDIUM JUMP ========\n", tolerance);
      }
      bfit = fitness(&cur, 1, len, items, &nada, cap, &pos, &pos, &pos);
   }
   else {
      tolerance = (tolerance > 0) ? tolerance + 1 : 1;
      tolerance = (tolerance > 2*tol_start) ? 2*tol_start : tolerance;
   }
   // add this move to all_hist
   for(i=0; i<c; i++) {
      j = out[i].sig;
      all_hist[i][ j ].freq ++;
      all_hist[i][ j ].sig = j;
      all_hist[i][ j ].density = out[i].density ;
   }

   // his[i%n]=cur
   memcpy(his[t%n], cur, sizeof(bool) * len);

   //fprintf(stderr,"\nFlipped %i, now fit %i\n", bpos, bfit);
   free(nada);
   return 0;
}

bool longTermJump(bool *cur, int len, hist_sig *out, hist_sig **all, int *items) {
   int c = len/ONESUBLEN + ( (len%ONESUBLEN) > 0);
   int i, j, k, z;

   bool n[ONESUBLEN];

   z = 1;
   for(i=0; i<ONESUBLEN; i++)
      z *= 2;
   // for all visited components
   for(i=0; i<c; i++) {
      // if previously visited
      if(all[i][ out[i].sig ].freq > 0) {
         // replace this segment with a random unvisited one
         j = rand() % z;
         while( all[i][j].freq > 0 )
            j = rand() % z;
         //fprintf(stderr, "============================== ONE LONG FLIP seg %i val %i\n", i, j);

         //for(k=0; k<ONESUBLEN && i*ONESUBLEN+k < len; k++) {
         //   cur[i*ONESUBLEN+k] = (j >> (ONESUBLEN-k-1)) & 1;
         //}
         out[i].sig = j;
         sigToSubstr(cur, len, &(out[i]), items, i*ONESUBLEN);
         
         //substrToSig(cur, len, out, items);
      }
   }
   return true;
}

bool mediumTermJump(bool *cur, int len, hist_sig *out, hist_sig **all, int flipped, int * items) {
   int c = flipped/ONESUBLEN;
   hist_sig *cat = all[c];

   bool med = false;
   int a, b, i, j;
   int cat_len = 1;
   for(i=0; i<ONESUBLEN; i++)
      cat_len *= 2;

   // if new component is particularly bad (non-dense), 
   // bad if less dense than previously visited combinations
   a = b = j = 0;
   for(i=0; i<cat_len; i++) {
      if(cat[i].freq > 0) {
         a += cat[i].density;
         b ++;
         if( cat[i].density > cat[j].density )
            j = i;
      }
   }
   if(b>0 && (float)a/b > out[c].density && out[c].density > 0) {
      //printf("========================= NOT DENSE ENOUGH, MED JUMP\n");
      med = true;
   }
   // if new component is previously visited (rejected before)
   //else if( cat[ out[c].sig ].freq > 1 ) {
   //   printf("========================= HISTORIC, MED JUMP\n");
   //   med = 1;
   //}
   // replace with a dense one from hist
   if(med) {
      out[c] = cat[j];
      sigToSubstr(cur, len, &(out[c]), items, c*ONESUBLEN);
   }
   return med;
}

bool compareState(bool *a, bool *b, int n) {
   int i;
   for(i=0; i<n; i++)
      if(a[i] != b[i])
         return false;
   return true;
}

int substrToSig(bool *cur, int len, hist_sig *out, int *items) {
   // for every 8 bits in cur (round up len/8)
   int c = len/ONESUBLEN + ( (len%ONESUBLEN) > 0);
   if (out==0 || out==NULL)
      out = (hist_sig*) malloc(sizeof(hist_sig) * c);
   int i, j;
   int val, wei;
   unsigned char tmp;
   for(i=0; i<c; i++) {
      tmp = 0;
      val = 0;
      wei = 0;
      // build 1 char
      for(j=0; j<ONESUBLEN && ONESUBLEN*i+j < len; j++) {
         if(cur[ONESUBLEN*i+j]) {
            tmp += (cur[ONESUBLEN * i + j]) << (ONESUBLEN-j-1);
            val += items[2*(ONESUBLEN*i+j)];
            wei += items[2*(ONESUBLEN*i+j)+1];
         }
      }
      // save 1 char
      out[i].sig = tmp;
      if(wei > 0) {
         out[i].density = val/wei;
      }
      else
         out[i].density = 0;
   }
   return 0;
}

int sigToSubstr(bool *cur, int len, hist_sig *out, int *items, int offset) {
   int val, wei;
   unsigned char tmp;
   int c = len/ONESUBLEN + ( (len%ONESUBLEN) > 0);

   int i = out->sig, j, k;
   for(k=0; k<ONESUBLEN && offset+k < len; k++) {
      j = (i >> (ONESUBLEN-k-1)) & 1;
      cur[offset+k] = (i >> (ONESUBLEN-k-1)) & 1;
   }

   for(i=0; i<c; i++) {
      tmp = 0;
      val = 0;
      wei = 0;
      // build 1 char
      for(j=0; j<ONESUBLEN && ONESUBLEN*i+j < len; j++) {
         if(cur[ONESUBLEN*i+j]) {
            tmp += (cur[ONESUBLEN * i + j]) << (ONESUBLEN-j-1);
            val += items[2*(ONESUBLEN*i+j)];
            wei += items[2*(ONESUBLEN*i+j)+1];
         }
      }
      // save 1 char
      out->sig = tmp;
      if(wei > 0) {
         out->density = val/wei;
      }
      else
         out->density = 0;
   }

   return 0;
}




