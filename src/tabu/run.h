#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "common.h"
#include "fitness.h"
#include "tabu.h"

int main(int, char **);

/*
 * bool* knapsack(int *a, int b, int d, int e, int f)
 *
 * Given an array of a[2i]=value and a[2i+1]
 * =weight on b items, use a tabu search
 * d generations and
 * return a boolean array of whether to include
 * each item.
 * By tabu search.
 *
 * @param   a  items w/ value,weight,value,weight...
 * @param   b  len(a)/2 (# of pairs)
 * @param   d  number of gens
 * @param   e  weight capacity
 * @param   f  length of short memory
 *
 * @return  bool* to chosen solution of length b/2
 */
bool* knapsack(int *, int, int, int, int);

/*
 * bool* initPop(int b)
 *
 * Given a chromosome
 * size b, randomly create an individual
 * with b boolean values. Return as
 * bool[a][b];
 *
 * @param   b  chromosome size
 * @return  bool[a][b] 2-d dyamic array of bools
 */
bool* initPop(int);


/*
 * int report(int *a, bool *b, int c, int d)
 *
 * Print a report for the results
 *
 * @param   a  [2*c], [2i]=item val, [2i+1]=item mass
 * @param   b  c booleans if item is included or not
 * @param   c  total items
 * @param   d  carrying capacity
 * @return  meaningless
 */
int report(int *, bool *, int, int);
