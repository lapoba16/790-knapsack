imgsz = ( 400, 100 )
import subprocess
def getstream(cmd) :
   stream = subprocess.Popen(cmd, shell=True,         \
      stdout=subprocess.PIPE, stderr=subprocess.PIPE, \
      universal_newlines=True                         \
   )
   return stream
def docmd(*args):
   full = ""
   if len(args) < 1 :
      return
   for i in args :
      full = full + " " + str(i)
   stream = getstream(full)
   return stream.communicate()

import matplotlib as mpl
mpl.use("Agg")

#  get num gens
gen = int(input())

x=[]
y=[]
f=[]
g=[]
z=[]
lfit=[]
lcnt=[]
items=[]

m = 0
#  read from run
for i in range(gen) :
   #  get oneline
   line = input()
   #  get ints; x=gen#, y=avgfit
   line = [ int(i) for i in line.split(' ') if len(i)>0 ]
   x.append(line[0])
   if line[1] > m :
      m = line[1]
   y.append(line[1])
   #  get worst non-1 and how many 1s
   line = input()
   line = [ int(i) for i in line.split(' ') if len(i)>0 ]
   lfit.append(line[0])
   lcnt.append(line[1])
   #  get global best and src gen
   line = input()
   line = [ int(i) for i in line.split(' ') if len(i)>0 ]
   if len(line) > 1 :
      f.append(line[0])
      g.append(line[1])
   #  get another line
   line = input()
   #  it's the leader of the gen
   line = [ int(i) for i in line.split(' ') if len(i)>0 ]
   z.append(line)

#  read item values and weights
for i in range(2) :
   line = input()
   line = [ int(i) for i in line.split(' ') if len(i) > 0 ]
   items.append(line)

#  draw plot of avg fitness over gens
import matplotlib
matplotlib.rcParams.update({'font.size':15})
import matplotlib.pyplot as plt
import sys
import math
bestingen = [ i[len(i)-1] for i in z ]
plt.xticks(range(min(x)//1, max(x)//1+5, int( (max(x)//1+5)/5 )) )
plt.ylim(0, max(bestingen)*1.1)
#plt.ylim(0, 62500)

#plt.plot(x, bestingen, label="Current State")

plt.plot(x, y, label="Average Individual")
plt.plot(x, bestingen, label="Best Individual")
plt.plot(x, lfit, label="Worst Individual")

plt.ylabel("Fitness")
plt.xlabel("Generation")
#plt.plot(x, lcnt, label="#Invalid")
plt.legend()
if len(sys.argv) > 1 :
   ttl = sys.argv[1]
else :
   ttl = "Tabu Search for the Fixed Set"
plt.title(ttl)
plt.subplots_adjust(left=0.19, right=0.97, top=0.93, bottom=0.13)
plt.savefig("out.pdf", format='pdf')
plt.close()

exit(0)

#  draw winner over time
import os
from PIL import Image
from PIL import ImageDraw

#  rm old stuff
docmd("rm -f ./winner*.png")

def imgPrintCentered(img, msg, x, y, color=0, fill=None) :
   draw = ImageDraw.Draw(img)
   textw, texth = draw.textsize(str(msg))

   if fill != None :
      poly = ( x - textw*.6,  y - texth*.6,
               x + textw*.6,  y - texth*.6,
               x + textw*.6,  y + texth*.6,
               x - textw*.6,  y + texth*.6,
      )
      dr.polygon( poly, fill )

   dr.text( (int(x - textw * .5)
      , int(y - texth * .5))
      , str(msg)
      , fill=int(color)
   )

#  for each winner, draw one frame
barPortion = 10
for k in range(len(z)) :
   i = z[k]
   #  sz dividable by num items
   sz = (imgsz[0] + imgsz[0]%(len(i)-1), imgsz[1])
   chunk = sz[0] / (len(i) - 1)
   tchunk = sz[0] / len(z)
   #  open image, 8bit png
   im = Image.new('L', sz, "white")
   dr = ImageDraw.Draw(im)
   #  draw each chunk x height block
   for j in range(len(i)-1) :
      poly = ( j*chunk,       0, 
         j*chunk + chunk,     0,
         j*chunk + chunk,     sz[1],
         j*chunk,             sz[1]
      )
      color = 255*i[j]*2
      cnt = 2
      if j > 0 :
         color += 255*i[j-1]
         cnt += 1
      if j < len(i)-1 :
         color += 255*i[j+1]
         cnt += 1
      color /= cnt
      dr.polygon( poly , int(color))
      if color < 127.5 :
         color = 255
      else :
         color = 0
      tw, th = dr.textsize(str(items[0][j]))
      imgPrintCentered(       im, 
         items[0][j],         j*chunk+chunk/2,
         sz[1]/2 - th,        color )
      imgPrintCentered(       im,
         items[1][j],         j*chunk+chunk/2,
         sz[1]/2     ,        color )
      imgPrintCentered(       im,
         i[j],                j*chunk+chunk/2,
         sz[1]/2 + th,        color )
   #  progress bar at the bottom
   poly = ( 0,       sz[1]-sz[1]/barPortion,
      k*tchunk+tchunk, sz[1]-sz[1]/barPortion,
      k*tchunk+tchunk, sz[1],
      0,             sz[1]
   )
   dr.polygon( poly, 225 )
   poly = ( 0,       sz[1]-(sz[1]/barPortion)*.8,
      k*tchunk+tchunk*.8, sz[1]-(sz[1]/barPortion)*.8,
      k*tchunk+tchunk*.8, sz[1],
      0,             sz[1]
   )
   dr.polygon( poly, 25 )
   #  write cur fitness, bg color white
   color = 0
   imgPrintCentered(       im,
      i[len(i)-1],         sz[0]/2,
      sz[1]/barPortion,             color, 255)
   #  write global fit, bg color white
   write=str(str(f[k])+" @ gen "+str(g[k]))
   imgPrintCentered(       im,
      write,         sz[0]/4,
      sz[1]/barPortion,             color, 255)

   #  save
   im.save("./winner{:03d}.png".format(k))
   im.close()


