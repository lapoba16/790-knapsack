import subprocess
import os
def getstream(cmd, getstr) :
   if getstr==True :
      stream = subprocess.Popen(cmd, shell=True,         \
         stdout=subprocess.PIPE, stderr=subprocess.PIPE, \
         universal_newlines=True                         \
      )
      return stream
   cmd = cmd.strip()
   os.system(cmd)
   return ""
   
def docmd(*args):
   args = [ i for i in args ]
   full = ""
   getstr = True
   if len(args) < 1 :
      return
   if args[len(args)-1]==False :
      getstr = False
      args.remove(args[len(args)-1])
   for i in args :
      full = full + " " + str(i)
   stream = getstream(full, getstr)
   if getstr :
      return stream.communicate()
   return

import matplotlib as mpl
import sys
mpl.use("Agg")

maxgen = 20000
steps = 4
otherparam = " 200 25 40 "
gpro = " gprof -b  -pzlookup -pbinsearchInterval -pgetPlusDeme -pdoRoulette -pprSelection -pbinsearchIntervalI -pSelection "
comp = []
#for z in [ -1, 0 ] :
   #for k in range(maxgen//steps, maxgen, maxgen//steps) :
for k in os.listdir('./results/plusDemeTiming/all/') :
   #docmd("echo \"{:d} {:s} {:d} -1\" > ./input && cat ./brutedInput486clip >> ./input".format(k, otherparam, z))
   #docmd("./sga < ./input > /dev/null 2> /dev/null")
   #output = docmd("{:s} ./sga ./gmon.out".format(gpro))[0]
   output = docmd("cat ./results/plusDemeTiming/all/{:s}".format(k))[0]
   output = [ j.split(' ') for j in [ i.strip() for i in output.split('\n') if len(i) > 0 and i.strip()[0].isnumeric() ] ]
   s = 0.0
   for i in range(len(output)) :
      output[i] = [ j for j in output[i] if len(j) > 0 ]
      output[i] = output[i][2]
      s += float(output[i])
   output = s
   #comp.append( (z, k, output) )
   comp.append( (  int(k.split('_')[0]) , int(k.split('_')[1]) , output ) )
   #print(comp[len(comp)-1])

comp = sorted(comp, key=lambda x : x[1] )
for i in comp :
   print(i)

#  draw plot of avg fitness over gens
import matplotlib.pyplot as plt
import sys
import math

plt.xlabel("Population Size")
plt.ylabel("Runtime for Selection Functions in Seconds from GProf")
mpl.rcParams.update({'font.size':13})
plt.title("Runtime Comparison of Fitness Proportional Selection")
plt.title("Runtime Comparison of Tournament Selection")
#plt.subplots_adjust(left=0.15, right=0.98, top=0.93, bottom=0.13)

#  plus
x = [ i[1] for i in comp if i[0] == 0 ]
y = [ i[2] for i in comp if i[0] == 0 ]
plt.plot(x, y, label="Plus Demes")

#  global
x = [ i[1] for i in comp if i[0] != 0 ]
y = [ i[2] for i in comp if i[0] != 0 ]
plt.plot(x, y, label="Global")

plt.legend()
plt.savefig("out.pdf", format="pdf")
plt.close()


