A simple genetic algorithm to solve generated knapsack problems.

#  Quickstart: 

`bash ./default [ #Pop [ #Gen [ Capacity [ #Items [ selSpace [ selType ]]]]]]`
   
Each argument is optional but order is fixed.

Argument |  Definition                    | Values
-------- | ------------------------------ | ----------
\#Pop    |  Population size               | >1
\#Gen    |  Generation count              | >0
Capcity  |  Knapsack carrying capacity    | >\#Items
\#Items  |  Number of items to generate   | >1
selSpace |  Who is eligible for selection | <0 for global, 0 for plus, >0 for radius
selType  |  Type of selection to use      | <1 for fitness proportional, >0 for tournament

---------------------------

`bash ./defaultWInput`

Runs a fixed medium sized problem with optimal fitness 524.

#  Less Quick Start:

`./gen #pop #gen capacity #items selSpace selType | ./sga | python3 ./results/plot.py "Chart Title"`

See ./out.pdf for the SGA results. Progress will be printed to stderr.

All arguments are optional and as defined above.

#  Slowstart:

`make`

Create the (gen)erator, (sga), (naive) knapsack, and (tabu) search programs.

--------------------------

`./gen #pop #gen cap #items #selCandidates selType`

All arguments are optional and as defined above.

Creates input in the appropriate format for `./sga` and `./naive`.

--------------------------

`./sga`

A simple genetic algorithm that takes stdin input in the following format. Whitespace is not strict.

> \#pop 
>
> \#gen 
>
> cap 
>
> \#item 
>
> selCandidates 
>
> selType 
>
> item1Val 
>
> item1Weight 
>
> item2Val 
>
> item2Weight
>
> ...

--------------------------

`./naive`

A dynamic programming solution to knapsack that takes the same inputs as sga.

--------------------------

`./tabu`

A tabu seach solution to knapsack takes slightly different input than ./gen gives. Whitespace is not strict.

>  \#gen 
>
>  cap 
>
>  \#item 
>
>  shortMemoryLength 
>
>  item1Val 
>
>  item1Weight 
>
>  item2Val 
>
>  item2Weight
>
>  ...